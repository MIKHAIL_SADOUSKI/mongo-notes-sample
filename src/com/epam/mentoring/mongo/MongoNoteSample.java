package com.epam.mentoring.mongo;

import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Date;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;

public class MongoNoteSample {

	public static void main(String[] args) {

		try {

			// connecting to MongoDB
			MongoClient mongo = new MongoClient("localhost", 27017);

			// getting database
			DB db = mongo.getDB("notes");

			// getting table
			DBCollection table = db.getCollection("note");

			// inserting rows
			BasicDBObject document1 = new BasicDBObject().append("createdDate", new Date()).append("tag", "products")
					.append("text", "Buy milk and chocolate");
			BasicDBObject document2 = new BasicDBObject().append("createdDate", new Date()).append("tag", "products")
					.append("text", "Buy bread");
			table.insert(Arrays.asList(document1, document2));

			// finding all rows and print
			table.find().forEach(System.out::println);

			// searching row by tag and print
			table.find(new BasicDBObject().append("tag", "products")).forEach(System.out::println);

			// searching row by tag and text and print
			table.find(new BasicDBObject().append("tag", "products").append("text", "Buy bread"))
					.forEach(System.out::println);

			// removing by criteria
			table.remove(new BasicDBObject().append("text", "Buy bread"));
			table.find().forEach(System.out::println);

		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (MongoException e) {
			e.printStackTrace();
		}

	}

}
